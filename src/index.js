import React, { Component } from 'react'; // create and manage components
import ReactDOM from 'react-dom'; // interact with DOM
import YTSearch from 'youtube-api-search';
import SearchBar from './components/search_bar';
import VideoList from './components/video_list';
import VideoDetail from './components/video_detail';
// Youtube API key
const API_KEY = 'AIzaSyDatAn16gx2FeRrooczk2oeu9skHOdLnQ4';

// Create a new component. This compnent should produce some HTML
// const declares a constant variable, i.e. cannot reassign App
// const App = () => { // value of this keyword different with fat arrow
//     // return <div>Hi!</div>;
//     return (
//         <div>
//             <SearchBar />
//         </div>
//     );
// };

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      videos: [],
      selectedVideo: null,
    };

    this.videoSearch('surfboards');
  }

  videoSearch(term) {
    YTSearch( // eslint-disable-line
      { key: API_KEY, term },
      (videos) => {
        this.setState({
          videos,
          selectedVideo: videos[0],
        });
        // this.setState({ videos: videos });
        // only works if key and value same var name
      });
  }

  render() {
    return (
      // passing props in React
      <div>
        <SearchBar onSearchTermChange = { term => this.videoSearch(term) } />
        <VideoDetail video = { this.state.selectedVideo } />
        <VideoList onVideoSelect = {
          selectedVideo => this.setState({
            selectedVideo,
          })}
          videos = { this.state.videos }
        />
      </div>
    );
  }
}

// Always one component per file

// Take this component's generated HTML and put it on the page (in the DOM)
// App is a class, but require instance
// ReactDOM.render(App);
// second argument is the target DOM element
ReactDOM.render(<App />, document.querySelector('.container'));
