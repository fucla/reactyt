import React, { Component } from 'react';
// equivalent to following
// const Component = React.Component;

// functional component
// const SearchBar = () => {
//     return <input />;
// };

// class component maintains state
class SearchBar extends Component {
  constructor(props) {
    super(props); // calls Component constructor
      // initialize state
      // term for 'search term'
    this.state = { term: '' };
  }
  onInputChange(term) {
    this.setState({ term });
    this.props.onSearchTermChange(term);
  }
    // naming: on + element + behavior
    // all browser events that get triggered are called with event object
    // onInputChange(event) {
    //     console.log(event.target.value);
    // }
  render() {
    return (
      <div className="search-bar">
        <input
          value={this.state.term} // controlled field
          onChange={event => this.onInputChange(event.target.value)}
        />
      </div>
    );
  }
}
SearchBar.propTypes = { onSearchTermChange: React.PropTypes.func };
export default SearchBar;
