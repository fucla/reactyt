import React from 'react';
import VideoListItem from './video_list_item';

// props argument for functional, props member for class
const VideoList = (props) => {
  // key is required for updates
  const videoItems = props.videos.map(video => { // eslint-disable-line
    return (
      <VideoListItem
        onVideoSelect={ props.onVideoSelect }
        key={ video.etag }
        video={ video }
      />
    );
  });

  return (
    <ul className="col-md-4 list-group">
      { videoItems }
    </ul>
  );
};
VideoList.propTypes = {
  videos: React.PropTypes.array,
  onVideoSelect: React.PropTypes.func,
};

export default VideoList;
